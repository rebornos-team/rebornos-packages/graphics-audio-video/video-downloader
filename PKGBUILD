# Maintainer:  Marcell Meszaros < marcell.meszaros AT runbox.eu >
# Contributor: Kevin Majewski < kevin.majewski02 AT gmail.com >
# Rafael from azul - depends added: gtk4 and libadwaita

pkgname='video-downloader'
pkgver=0.10.11
pkgrel=1
pkgdesc='GTK application to download videos from websites like YouTube and many others (based on yt-dlp)'
arch=('any')
url="https://github.com/Unrud/${pkgname}"
license=('GPL3')
depends=(
  'dconf'
  'gtk3'
  'hicolor-icon-theme'
  'libhandy'
  'python-gobject'
  'yt-dlp'
  'gtk4'
  'libadwaita'
)
makedepends=(
  'librsvg'
  'meson'
)
_tarname="${pkgname}-${pkgver}"
source=("${_tarname}.tar.gz::${url}/archive/refs/tags/v${pkgver}.tar.gz")
b2sums=('db546a81f41255cab649c2ebeefd2e439fc2af2f72ea6dc2fe367f35b27e5be87a9e5ff308b35af80b4651b1ef254780be7c2b53e12568b2c78876e0c2ac30ea')

prepare() {
  arch-meson "${_tarname}" 'build'
}

build() {
  meson compile -C 'build'
}

check() {
  meson test -C 'build' --print-errorlogs || warning 'There were test failures'
}

package() {
  # extra depends are needed for yt-dlp - some are mandatory but erroneously set to optional in Arch
  depends+=(
    'ffmpeg'
    'python-brotli'
    'python-mutagen'
    'python-pycryptodomex'
    'python-websockets'
    'python-xattr'
  )

  DESTDIR="${pkgdir}" meson install -C 'build'
}
